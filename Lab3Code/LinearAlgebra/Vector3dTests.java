// Mykyta Onipchenko
// 2034746

package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;


public class Vector3dTests {
    private static final double EPSILON = 0.0001;

    @Test
    public void TestVector3dConstructor() {
        Vector3d vec = new Vector3d(1.2, 1.6, 2.8);
        assertEquals(1.2, vec.getX(), EPSILON);
        assertEquals(1.6, vec.getY(), EPSILON);
        assertEquals(2.8, vec.getZ(), EPSILON);
    }

    @Test
    public void TestVector3dMagnitude() {
        Vector3d vec = new Vector3d(1.2, 1.6, 2.8);
        assertEquals(3.44093, vec.magnitiude(), EPSILON);
    }

    @Test
    public void TestVector3dDot() {
        Vector3d vec = new Vector3d(1.2, 1.6, 2.8);
        Vector3d other = new Vector3d(17, -2.7, 71);
        assertEquals(214.88, vec.dotProduct(other), EPSILON);
    }

    @Test
    public void TestVector3dAdd() {
        Vector3d vec = new Vector3d(1.2, 1.6, 2.8);
        Vector3d other = new Vector3d(17, -2.7, 71); 

        Vector3d result = vec.add(other);

        assertEquals(18.2, result.getX(), EPSILON);
        assertEquals(-1.1, result.getY(), EPSILON);
        assertEquals(73.8, result.getZ(), EPSILON);
    }
}
